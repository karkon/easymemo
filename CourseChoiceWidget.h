/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef COURSECHOICEWIDGET_H
#define COURSECHOICEWIDGET_H

#include "Course.h"
#include <QWidget>
#include <QInputDialog>
#include <QMessageBox>
#include <qlistwidget.h>
#include"FileUtility.h"
#include <QMessageBox>
#include <QFileDialog>

namespace Ui {
class CourseChoiceWidget;
}
/**
 * @brief CourseChoiceWidget - GUI class describing Widget allowing to choose which course we want to repeat
 */
class CourseChoiceWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief CourseChoiceWidget consctructor
     */
    explicit CourseChoiceWidget(QWidget *parent = nullptr);
    /**
     * @brief CourseChoiceWidget desctructor
     */
    ~CourseChoiceWidget();
    /**
     * @brief function loads course list from file;
     */
    void LoadCourseList();
signals:
    /**
     * @brief choosen course signal
     * @param path - filename of cosen course
     */
    void courseChoiceSignal(const QString path);
public slots:
    /**
     * @brief Slot loads all Course list from file
     */
    void LoadCourseListSlot();

private slots:
    /**
     * @brief Repeat Button slot, starts selected course repetiction
     */
    void on_RepeatBtn_clicked();
    /**
     * @brief AddFromFile Button slot, loads new course from file - UNUSED
     */
    void on_AddFromFileBtn_clicked();
    /**
     *
     * @brief MixedRepead Button slot, starts mixed repetiction  - UNUSED
     */
    void on_MixedRepeadBtn_clicked();
private:
    /**
     * @brief CourseChoiceWidget UI instance
     */
    Ui::CourseChoiceWidget *ui;
    /**
     * @brief courseList vector containg list of all courses wih data of them.
     */
    std::vector<Course> courseList;

};

#endif // COURSECHOICEWIDGET_H
