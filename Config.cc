/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "Config.h"

Config::Config(){}
Config::Config(std::string file_path):configFilePath(file_path){}

void Config::loadConfig(std::string file_path)
{
    configFilePath=file_path;
    //Possible extension of config data
    loadCoursesList("courseslist.xml");
}

void Config::loadCoursesList(std::string file_path){
    configFilePath=file_path;
    FileUtility file(QString::fromStdString(configFilePath), "courseslist");
    learningCourses = file.getCoursesListFromFile();
}

void Config::setNumberOfCourses(int number_of_courses)
{
    numberOfCourses=number_of_courses;
}

int Config::getNumberOfCourses()
{
    return numberOfCourses;
}

void Config::setTotalLearningTime(QTime total_learning_time)
{
    totalLearningTime=total_learning_time;
}

QTime Config::getTotalLearningTime()
{
    return totalLearningTime;
}

std::string Config::getConfigFilePath()
{
    return configFilePath;
}

void Config::setCoursesListFilePath(std::string courses_list_file_path)
{
    coursesListFilePath=courses_list_file_path;
}

std::string Config::getCoursesListFilePath()
{
    return coursesListFilePath;
}
