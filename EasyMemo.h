/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "CourseWidget.h"
#include "CourseChoiceWidget.h"
#include "StatisticWidget.h"
#include "CreateCourseWidget.h"

namespace Ui {
class EasyMemo;
}
/**
 * @brief  EasyMemo - GUI class inheriting from QMainWindow, the main window of the program
 */
class EasyMemo : public QMainWindow
{
    Q_OBJECT

public:
    /**
     * @brief EasyMemo Constructor
     */
    explicit EasyMemo(QWidget *parent = nullptr);
    /**
     * @brief EasyMemo construktor
     */
    ~EasyMemo();
public slots:
    /**
     * @brief
     */
    void runCourse( const QString path );
    /**
     * @brief Slot acivated by signal from CourseWidget, changes currently displayed page
     */
    void OnchangeAcceptedSlot(int page);
signals:
    /**
     * @brief signal
     * @param path filepath with course file we want to repeat
     */
    void runCourseSignal(const QString path);
    /**
      * @brief signal informing CourseChoice Widget to load courses list
      */
    void loadCourseListSignal();
    /**
      * @brief signal
      * @param page tells number of page on which we want to change
      */
    void changeWhileCourseOnlineSignal(const int page);
private slots:
    /**
      * @brief course button slot, showing us course choice  page
      */
    void on_courseBtn_clicked();
    /**
     * @brief addCourse button slot, showing us create course  page
     */
    void on_addCourseBtn_clicked();
    /**
     * @brief statistic button slot, showing us statistic page
     */
    void on_statisticBtn_clicked();

private:
    /**
     * @brief Main program window UI instance
     */
    Ui::EasyMemo *ui;
    /**
     * @brief CourseWidget instance
     */
    CourseWidget coursePage;
    /**
     * @brief  CourseChoiceWidget instance
     */
    CourseChoiceWidget courseChoicePage;
    /**
     * @brief  StatisticWidget instance
     */
    StatisticWidget statisticsPage;
    /**
     * @brief CreateCourseWidget instance
     */
    CreateCourseWidget createCourse;
    /**
     * @brief bool value specifying if one of the courses is currently repeated
     */
    bool isCourseActive;
};

#endif // MAINWINDOW_H

