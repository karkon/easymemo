/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef COURSEWIDGET_H
#define COURSEWIDGET_H

#include"string.h"
#include "QDebug"
#include <QShortcut>
#include <QWidget>
#include"MemoCourse.h"
#include "Algorithm.h"
#include "Config.h"
#include <QMessageBox>

namespace Ui {
class CourseWidget;
}
/**
 * @brief CourseWidget - GUI CLass describing Widget which allows us to repeat Memocourse
 */
class CourseWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief CourseWidget constructor
     */
    explicit CourseWidget(QWidget *parent = nullptr);
    /**
     * @brief CourseWidget destructor
     */
    ~CourseWidget();
public slots:
    /**
     * @brief function loads course from xml file specified in path variable
     * @param path is course filepath
     */
    void loadCourse( const QString path );

    /**
     * @brief Slot called when we want to change page during the course revision
     * @param page - page index on which we want to change
     */
    void onChangeSlot( const int page);


signals:
    /**
     * @brief Signal slot, connected with EasyMemo Main window, provides informactio than user confirmed ending of a repetiction
     * @param page tells number of page on which we want to change
     */
    void PageChangeAccepttedSignal(const int page);

private slots:
    /**
     * @brief Show Button slot, it shows meaning of the word
     */
    void on_showBtn_clicked();
    /**
     * @brief Bad Button slot, sets quality 1
     */
    void on_badBtn_clicked();
    /**
     * @brief medium Button slot, sets quality 3
     */
    void on_mediumbtn_clicked();
    /**
     * @brief medium Button slot, sets quality 5
     */
    void on_goodBtn_clicked();
    /**
     * @brief timer slot, it refresh timer on the course page
     */
    void showTime();
private:
    /**
     * @brief Course Widget UI instance
     */
    Ui::CourseWidget *ui;
    /**
     * @brief MemoCourse Instance containing current course
     */
    MemoCourse currentCourse;
    /**
     * @brief current memocard index
     */
    int currentMemoCard;
    /**
     * @brief bool value specifying first/second repetition, false - first repetition
     */
    bool secondRepetiction;
    /**
     * @brief bool value specifynig if show btn was pressed, it blocks multiple presses on good/medium/bad Button
     */
    bool isShowButtonPressed;
    /**
     * @brief Algorithm class instance
     */
    Algorithm* algorithm=nullptr;
    /**
     * @brief Shortcut for "3" key, calling on_goodBtn_clicked()
     */
    QShortcut   *good;
    /**
     * @brief Shortcut for "1" key, calling on_badBtn_clicked()
     */
    QShortcut   *bad;
    /**
     * @brief Shortcut for "2" key, calling on_mediumBtn_clicked()
     */
    QShortcut   *medium;
    /**
     * @brief Shortcut for "SPACE" key, calling on_showBtn_clicked()
     */
    QShortcut   *show;
    /**
     * @brief Qtimer class instace, counting 1 second
     */
    QTimer *timer;
    /**
     * @brief Qtime class instance
     */
    QTime time;
    /**
     * @brief function caluclates quality value in second repetiction
     * @param tmpQuality - temporary quality value, used to calculate quality in second repetiction
     */
    void CalculateQuality(int tmpQuality);
    /**
     * @brief function finishes repetiction, saves course to file
     */
    void endOfRepetiction();
    /**
     * @brief function prepares second repetiction and chcec if memocard should be repeted second time (memocard.quality != 5)
     */
    void prepareSecondRepetiction();
    /**
     * @brief function loads next memocard
     * @param quality quality of current repetition.
     */
    void loadNextMemoCard(int quality);
    /**
     * @brief function checks if next memocard should be repeted or not
     */
    void checkRepeticion();
};

#endif // COURSEWIDGET_H
