/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef ALGORITHM_H
#define ALGORITHM_H
#include <MemoCard.h>
#include "QDebug"
/**
 * @brief The Algorithm class. Class responsible for managing the appointing repetitions operations.
 */

class Algorithm
{
public:
    /**
     * @brief getInstance allows to get instance of class Algorithm.
     * @return static Algorithm instance.
     */
    static Algorithm& getInstance();
    /**
     * @brief calculateNextRepetition main function responsible for calculating the next repetition day.
     * @return number of days until the next repetition.
     */
    void calculateNextRepetition(MemoCard &memo, int &quality);
    /**
     * @brief countEF calculate the new value of an E-Factor.
     * @param oldEF old value of the E-Factor, before repeating.
     * @param quality quality of the response
     * @return new value of E-Factor
     */
    double countEF(double const &old_EF, int &quality);


private:
    /**
     * @brief Algorithm default constructor.
     */
    Algorithm();
    /**
     * @brief Algorithm
     */
    Algorithm(const Algorithm&) = delete;
    /**
     * @brief operator =
     * @return return algorithm instance
     */
    Algorithm& operator=(const Algorithm&) = delete;

};

#endif // ALGORITHM_H
