/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef COURSE_H
#define COURSE_H
#include <string>
#include<vector>
#include<ctime>
#include<QTime>

/**
 * @brief The Course class cointains one course on any type.
 */
class Course
{
public:
    /**
     * @brief Course default constructor.
     */
    Course();
    /**
     * @brief Course parametric constructor.
     * @param file_path file path to a XML file containing elements of the course.
     * @param course_name name of course.
     */
    Course(std::string file_path, std::string course_name);
    /**
     * @brief getFilePath allows to get file path of XML file containing elements of course.
     * @return path of XML file.
     */
    std::string getFilePath();
    /**
     * @brief getCourseName allows to get name of the course;
     * @return name of the course.
     */
    std::string getCourseName();
    /**
     * @brief setFilePath allows to set file path of XML file containing elements of course.
     * @param file_path path of XML file.
     */
    void setFilePath(std::string file_path);
    /**
     * @brief setCourseName allows to set name of the course.
     * @param course_name new name of the course.
     */
    void setCourseName(std::string course_name);
    /**
     * @brief setLearningTime allows to set learning time of one course.
     * @param learning_time learning time of one course.
     */
    void setLearningTime(QTime learning_time);
    /**
     * @brief getLearningTime allows to get learning time of one course.
     * @return learning time of one course.
     */
    QTime getLearningTime();
    /**
     * @brief setNumberOfCards allows to set number of cards in one course.
     * @param number_of_cards new number of cards in one course.
     */
    void setNumberOfCards(int number_of_cards);
    /**
     * @brief getNumberOfCards allows to get number of cards in one course.
     * @return number of cards in one course.
     */
    int getNumberOfCards();
    /**
     * @brief getCourse allows to get the indicator to whole course.
     * @return indicator to whole course.
     */
    Course *getCourse();
protected:
    /**
     * @brief courseName name of the course.
     */
    std::string courseName;
    /**
     * @brief courseFilePath string path to the file.
     */
    std::string courseFilePath;
    /**
     * @brief learningTime learning time of one course.
     */
    QTime learningTime;
    /**
     * @brief numberOfCards number of cards in one course.
     */
    int numberOfCards;
};

#endif // COURSE_H
