/**
  *@author Karolina Kondzielnik, Michal Trendak
  */
#include "CreateCourseWidget.h"
#include "ui_CreateCourseWidget.h"

#include <QDebug>


CreateCourseWidget::CreateCourseWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CreateCourseWidget)
{
    ui->setupUi(this);
    tableViewModel = new QStandardItemModel(ui->CourseTable);
    ui->CourseTable->setModel(tableViewModel);
    tableViewModel->setColumnCount(2);
    tableViewModel->setHorizontalHeaderLabels({"Słowo", "Znaczenie"});
    ui->CourseTable->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->CourseTable->setSelectionMode(QAbstractItemView::SingleSelection);
}

CreateCourseWidget::~CreateCourseWidget()
{
    delete ui;
}

void CreateCourseWidget::on_AddMemoCardBtn_clicked()
{
    int index = tableViewModel->rowCount();
    QStandardItem *test = new QStandardItem("słowo");
    tableViewModel->setItem(index,0,test);
    QStandardItem *test1 = new QStandardItem("znaczenie");
    tableViewModel->setItem(index,1,test1);
}

void CreateCourseWidget::on_SaveBtn_clicked()
{
    Course tmpCourse;
    QModelIndex index;
    MemoCourse tmpMemoCourse;
    tmpCourse.setFilePath(ui->textEdit->toPlainText().toStdString()+".xml");
    tmpCourse.setCourseName(ui->textEdit->toPlainText().toStdString());
    tmpMemoCourse.setFilePath(ui->textEdit->toPlainText().toStdString()+".xml");
    tmpMemoCourse.setCourseName(ui->textEdit->toPlainText().toStdString()); //jeszcze zapisz do kursu ile jest memokart
    for ( int row = 0; row <  tableViewModel->rowCount(); ++row )
    {
        MemoCard tmpCard;
        index = tableViewModel->index( row, 0 );
        tmpCard.setWord(tableViewModel->data(index).toString().toStdString());
        index = tableViewModel->index( row, 1 );
        tmpCard.setMeaning(tableViewModel->data(index).toString().toStdString());
        tmpMemoCourse.addMemoCard(tmpCard);
    }

    FileUtility fileutility(QString::fromStdString(tmpMemoCourse.getFilePath()), "memocards");
    int i;
    for(i=0;i<=tmpMemoCourse.ReturnSize()-1;i++){
        fileutility.addMemoCardToFile(tmpMemoCourse.getMemoCard(i));
    }
    fileutility.saveToFile();

    FileUtility fileutilitySecond(QString::fromStdString("courseslist.xml"), "courseslist");
    fileutilitySecond.addCourse(&tmpMemoCourse);
    fileutilitySecond.saveToFile();
    ui->textEdit->clear();
    tableViewModel->clear();
}


void CreateCourseWidget::on_DeleteMemocardBtn_clicked()
{
    QModelIndexList selection =  ui->CourseTable->selectionModel()->selectedRows();
    int row;
    foreach (QModelIndex index, selection) {
        row = index.row();
        tableViewModel->removeRows(row,1,QModelIndex());
    }

}
