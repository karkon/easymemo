/**
  *@author Karolina Kondzielnik, Michal Trendak
  */
#ifndef CREATECOURSEWIDGET_H
#define CREATECOURSEWIDGET_H

#include <QStandardItemModel>
#include <QWidget>
#include "MemoCard.h"
#include "MemoCourse.h"
#include "Course.h"
#include "FileUtility.h"

namespace Ui {
class CreateCourseWidget;
}
/**
 * @brief CreateCourseWidget GUI class describing widget allowing us to create new course
 */
class CreateCourseWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief  CreateCourseWidget constructor
     */
    explicit CreateCourseWidget(QWidget *parent = nullptr);
    /**
     * @brief CreateCourseWidget destructor
     */
    ~CreateCourseWidget();

private slots:
    /**
     * @brief AddMemoCard Button slot, adds new row to the CourseTable
     */
    void on_AddMemoCardBtn_clicked();
    /**
     * @brief Save Button slot, saves Course to the file
     */
    void on_SaveBtn_clicked();
    /**
     * @brief DeleteMemoCard Button slot, deletes selected row in the CourseTable
     */
    void on_DeleteMemocardBtn_clicked();

private:
    /**
     * @brief CreateCourseWidget UI instance
     */
    Ui::CreateCourseWidget *ui;
    /**
     * @brief CourseTable item model
     */
    QStandardItemModel *tableViewModel;

};

#endif // CREATECOURSEWIDGET_H
