/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "CourseWidget.h"
#include "ui_CourseWidget.h"

#include <QMessageBox>



CourseWidget::CourseWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CourseWidget)
{
    algorithm->getInstance();
    ui->setupUi(this);
    ui->badBtn->hide();
    ui->goodBtn->hide();
    ui->mediumbtn->hide();
    ui->meaningLabel->hide();
    ui->progressBar->setValue(0);
    good= new QShortcut(QKeySequence(Qt::Key_1), this, SLOT(on_badBtn_clicked()));
    bad = new QShortcut(QKeySequence(Qt::Key_2), this, SLOT(on_mediumbtn_clicked()));
    medium = new QShortcut(QKeySequence(Qt::Key_3), this, SLOT(on_goodBtn_clicked()));
    show = new QShortcut(QKeySequence(Qt::Key_Space), this, SLOT(on_showBtn_clicked()));
    timer = new QTimer(this);
    connect(timer, &QTimer::timeout, this, &CourseWidget::showTime);
}

CourseWidget::~CourseWidget()
{
    delete ui;
}
void CourseWidget::loadCourse(const QString path){
	ui->progressBar->setValue(0);
    currentMemoCard=0;
    secondRepetiction=false;
    isShowButtonPressed=false;
    currentCourse = MemoCourse(path.toStdString(), "kursTestowy");
    currentCourse.findRepetitions();
    ui->wordLabel->setText(QString::fromStdString(currentCourse.getMemoCard(currentMemoCard)->getWord()));
    ui->mediumbtn->setDisabled(0);
    ui->showBtn->show();
    ui->Timer->display("00:00");
    time =  QTime(0,0,0);
    timer->start(1000);

}


void CourseWidget::on_showBtn_clicked()
{   if(currentMemoCard<=currentCourse.ReturnSize()-1){
        ui->badBtn->show();
        ui->goodBtn->show();
        ui->mediumbtn->show();
        ui->showBtn->hide();
        ui->meaningLabel->show();
        ui->meaningLabel->setText(QString::fromStdString(currentCourse.getMemoCard(currentMemoCard)->getMeaning()));
        isShowButtonPressed=true;
    }
}

void CourseWidget::on_badBtn_clicked()
{
    if(isShowButtonPressed==true) {
        if(secondRepetiction==false)

            this->loadNextMemoCard(1);

        else
            this->CalculateQuality(1);
        isShowButtonPressed=false;
    }
}

void CourseWidget::on_mediumbtn_clicked()
{   if(isShowButtonPressed==true) {
        if(secondRepetiction==false)
            this->loadNextMemoCard(3);
        isShowButtonPressed=false;
    }
}

void CourseWidget::on_goodBtn_clicked()
{    if(isShowButtonPressed==true) {
        if(secondRepetiction==false)
            this->loadNextMemoCard(5);

        else
            this->CalculateQuality(5);
        isShowButtonPressed=false;
    }
}
void CourseWidget::loadNextMemoCard(int quality){
    currentCourse.getMemoCard(currentMemoCard)->setQuality(quality);
    algorithm->calculateNextRepetition(*currentCourse.getMemoCard(currentMemoCard),quality);
    ++currentMemoCard;
    checkRepeticion();

}
void CourseWidget::checkRepeticion(){
    if(currentCourse.getMemoCard(currentMemoCard)->getRandomOrderFactor()!=-10){
        ui->progressBar->setValue( static_cast<int>(1.0*currentMemoCard/currentCourse.ReturnSize()*100));
        ui->badBtn->hide();
        ui->goodBtn->hide();
        ui->mediumbtn->hide();
        ui->showBtn->show();
        ui->meaningLabel->hide();
        if(currentMemoCard<=currentCourse.ReturnSize()-1)
            ui->wordLabel->setText(QString::fromStdString(currentCourse.getMemoCard(currentMemoCard)->getWord()));
        else if (secondRepetiction==false){
            secondRepetiction=true;
            currentMemoCard=0;
            ui->mediumbtn->setDisabled(1);
            prepareSecondRepetiction();
        }
        else{
            this->endOfRepetiction();
        }
    }
    else{
        ++currentMemoCard;
        checkRepeticion();
    }

}
void CourseWidget::CalculateQuality(int tmpQuality)
{   if(currentMemoCard<=currentCourse.ReturnSize()-1)
    {
        if(currentCourse.getMemoCard(currentMemoCard)->getQuality()==5 && currentCourse.getMemoCard(currentMemoCard)->getRandomOrderFactor()!=-10 )
        {
            ++currentMemoCard;
            CalculateQuality( tmpQuality);
        }
        else
        {
            if(currentCourse.getMemoCard(currentMemoCard)->getQuality()<tmpQuality)
            {
                currentCourse.getMemoCard(currentMemoCard)->increaseQuality();
                loadNextMemoCard( currentCourse.getMemoCard(currentMemoCard)->getQuality());
            }
            else
            {
                currentCourse.getMemoCard(currentMemoCard)->decreaseQuality();
                loadNextMemoCard( currentCourse.getMemoCard(currentMemoCard)->getQuality());
            }
        }
    }
    else{
        ui->wordLabel->setText("koniec Kursu");
        this->endOfRepetiction();
    }

}
void CourseWidget::onChangeSlot( const int page){
    if(QMessageBox::question(this, "UWAGA !", "Czy na pewno chcesz przerwac powtórke ?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
    {
        emit PageChangeAccepttedSignal(page);
        this->endOfRepetiction();
    }
}
void CourseWidget::endOfRepetiction()
{
    timer->stop();
    ui->wordLabel->setText("koniec Kursu");
    ui->meaningLabel->setText("");
    ui->badBtn->hide();
    ui->goodBtn->hide();
    ui->mediumbtn->hide();
    ui->showBtn->hide();
    currentCourse.editAndSave(currentCourse.getFilePath());

}
void CourseWidget::showTime()
{
    time.setHMS(0,time.addSecs(+1).minute(),time.addSecs(+1).second());
    QString text = time.toString("mm:ss");
    if ((time.second() % 2) == 0)
        text[2] = ' ';
    ui->Timer->display(text);
}
void CourseWidget::prepareSecondRepetiction(){
    if(currentMemoCard<=currentCourse.ReturnSize()-1)
    {
        if(currentCourse.getMemoCard(currentMemoCard)->getQuality()==5)
        {
            ++currentMemoCard;
            prepareSecondRepetiction();
        }
        else
        {
            ui->wordLabel->setText(QString::fromStdString(currentCourse.getMemoCard(currentMemoCard)->getWord()));
            ui->badBtn->hide();
            ui->goodBtn->hide();
            ui->mediumbtn->hide();
            ui->showBtn->show();
            ui->meaningLabel->hide();
        }

    }
    else{
        this->endOfRepetiction();
    }


}
