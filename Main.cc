/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#include "EasyMemo.h"
#include "FileUtility.h"
#include <QApplication>
#include <Config.h>
#include <ctime>

int main(int argc, char *argv[])
{
    qDebug()<<"----------------------------------------------------------------------------------";
    qDebug()<<"Start of the program";
    qDebug()<<"----------------------------------------------------------------------------------";
    QApplication a(argc, argv);
    EasyMemo w;
    w.show();
   // Config config;
    //config.loadConfig("config.xml");
    qDebug()<<"----------------------------------------------------------------------------------";
    qDebug()<<"End of the program";
    qDebug()<<"----------------------------------------------------------------------------------";

    return a.exec();
}
