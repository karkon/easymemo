/**
  *@author Karolina Kondzielnik, Michal Trendak
  */
#ifndef STATISTICWIDGET_H
#define STATISTICWIDGET_H

#include <QWidget>

namespace Ui {
class StatisticWidget;
}
/**
 * @brief StatisticWidget  GUI class showing learning statistics
 */
class StatisticWidget : public QWidget
{
    Q_OBJECT

public:
    /**
     * @brief Statistic Widget constructor
     */
    explicit StatisticWidget(QWidget *parent = nullptr);
    /**
     * @brief Statistic Widget desctructor
     */
    ~StatisticWidget();

private:
    /**
     * @brief StatisticWidget  UI instance
     */
    Ui::StatisticWidget *ui;
};

#endif // STATISTICWIDGET_H
