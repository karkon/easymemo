/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef MEMOCARD_H
#define MEMOCARD_H

#include<string>
#include<QDate>


/**
 * @brief The MemoCard class. Class represents z memocard in the form of a word and meaning pair.
 */
class MemoCard
{
public:
    /**
     * @brief MemoCard default constructor.
     */
    MemoCard();
    /**
     * @brief MemoCard parametric constructor.
     * @param word word to learn.
     * @param meaning the meaning of the word.
     */
    MemoCard(std::string &word, std::string &meaning);
    /**
     * @brief setWord allows to change the word to learn.
     * @param new_word new word to learn.
     */
    void setWord(std::string new_word);
    /**
     * @brief setMeaning allows to change the meaning of a word.
     * @param new_meaning new meaning of a word.
     */
    void setMeaning(std::string new_meaning);
    /**
     * @brief setEF allows to change the E-Factor.
     * @param new_ef new E-Factor value;
     */
    void setEF(double new_ef);
    /**
     * @brief getWord allows to get a word to learn.
     * @return string word to learn.
     */
    std::string getWord();
    /**
     * @brief getMeaning allows to get the meaning of a word.
     * @return string meaning of the word.
     */
    std::string getMeaning();
    /**
     * @brief getEF allows to get the E-Factor.
     * @return float E-Factor.
     */
    double getEF();
    /**
     * @brief getRepetitionsNumber allows to get number of repetitions.
     * @return number of repetitions.
     */
    int getRepetitionsNumber();
    /**
     * @brief increaseRepetitionsNumber function increasing number of repetitions from the beginning of the learning process.
     */
    void increaseRepetitionsNumber();
    /**
     * @brief setRepetitionsNumber allows to set number of repetitions.
     * @param number number of repetitions.
     */
    void setRepetitionsNumber(int number);
    //    /**
    //     * @brief getIsRepeated allows to get bool value specyfying if memocard is repeated or not.
    //     * @return bool value specyfying if memocard is repeated or not.
    //     */
    //    bool getIsRepeated();
    //    /**
    //     * @brief setIsRepeated allows to set value specyfying if memocard is repeated or not.
    //     * @param newIsRepeated new value specyfying if memocard is repeated or not.
    //     */
    //    void setIsRepeated(bool new_is_repeated);
    /**
     * @brief getNextRepetition allows to get number of days until the next repetition.
     * @return number of days until the next repetition.
     */
    int getNextRepetition();
    /**
     * @brief setNextRepetition set number of days until the next repetition.
     */
    void setNextRepetition(int new_next_repetition);
    /**
     * @brief getQuality allows to get quality of current repetition.
     * @return quality of current repetition.
     */
    int getQuality();
    /**
     * @brief setQuality allows to set quality of current repetition.
     * @param newQuality new value of quality.
     */
    void setQuality(int new_quality);
    /**
     * @brief function allows to increase quality value by 1
     */
    void increaseQuality();
    /**
     * @brief function allows to decrease quality value by 1
     */
    void decreaseQuality();
    /**
     * @brief function returns memocard pointer
     */
    MemoCard* getMemoCard();
    /**
     * @brief setRandomOrderFactor allows to set random order factor of one memo card.
     * @param random_order_factor new random order factor.
     */
    void setRandomOrderFactor(int random_order_factor);
    /**
     * @brief getRandomOrderFactor allows to get random order factor of one memo card.
     * @return random order factor of one memo card.
     */
    double getRandomOrderFactor();
    /**
     * @brief setNextRepetitionDate allows to set date of the next repetition of one memo card.
     * @param next_repetition_date new date of the next repetition
     */
    void setNextRepetitionDate(QDate next_repetition_date);
    /**
     * @brief getNextRepetitionDate allows to get date of the next repetition of one memo card.
     * @return date of the next repetition of one memo card.
     */
    QDate getNextRepetitionDate();
    /**
     * @brief editNextRepetitionDate allows to count the next repetition Date using the number of days to the next repetition.
     * @param next_repetition number of days to the next repetition.
     */
    void editNextRepetitionDate(int next_repetition);

private:
    /**
    * @brief word word to learn.
    */
    std::string word;
    /**
    * @brief meaning meaning of the word to learn.
    */
    std::string meaning;
    /**
    * @brief ef easiness factor reflecting the easiness of memrizing and retaining a given item in memory.
    */
    double ef;
    /**
    * @brief repetitionsNumber number of repetitions.
    */
    int repetitionsNumber;
    /**
    * @brief randomOrderFactor
    */
    double randomOrderFactor;
    /**
    * @brief nextRepetition determines how many days until the next replay.
    */
    int nextRepetition;
    /**
    * @brief quality quality of current repetition.
    */
    int quality;
    // bool isRepeated; //czy to wgl jest potrzebne do czegos? do tego sa gettery i settery
    /**
    * @brief nextRepetitionDate date of the next repetition of the one card.
    */
    QDate nextRepetitionDate;
};

#endif // MEMOCARD_H
