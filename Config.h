/**
  *@author Karolina Kondzielnik, Michal Trendak
  */

#ifndef CONFIG_H
#define CONFIG_H
#include<vector>
#include<string>
#include<ctime>
#include "Course.h"
#include "FileUtility.h"

/**
 * @brief The Config class responsible for loading from the file and storing cofiguration data.
 */
class Config
{
public:
    /**
     * @brief Config default constructor.
     */
    Config();
    /**
     * @brief Config parameter constructor, loads configuration data.
     * @param file_path string cofiguration file path.
     */
    Config(std::string file_path);
    /**
     * @brief LoadConfig loads config data
     * @param file_path path to XML config file.
     */
    void loadConfig(std::string file_path);
    /**
     * @brief loadCoursesList loads list of courses with parameters.
     * @param file_path path to XML file.
     */
    void loadCoursesList(std::string file_path);
    /**
     * @brief setNumberOfCourses allows to set number of available courses to learn.
     * @param number_of_courses new available number of courses to learn.
     */
    void setNumberOfCourses(int number_of_courses);
    /**
     * @brief getNumberOfCourses allows to get number of available courses to learn.
     * @return available number of courses to learn.
     */
    int getNumberOfCourses();
    /**
     * @brief setTotalLearningTime allows to set total learning time of all available courses.
     * @param total_learning_time new total learning time.
     */
    void setTotalLearningTime(QTime total_learning_time);
    /**
     * @brief getTotalLearningTime allows to get total learning time of all available courses.
     * @return total learning time.
     */
    QTime getTotalLearningTime();
    /**
     * @brief getConfigFilePath allows to get file path with configuration data.
     * @return configuration data file path.
     */
    std::string getConfigFilePath();
    /**
     * @brief setCoursesListFilePath allows to set file path with courses list.
     * @param courses_list_file_path new courses list file path.
     */
    void setCoursesListFilePath(std::string courses_list_file_path);
    /**
     * @brief getCoursesListFilePath allows to get file path with courses list.
     * @return courses list file path.
     */
    std::string getCoursesListFilePath();
private:
    /**
     * @brief learningCourses vector of course names whose learning have been started.
     */
    std::vector<Course> learningCourses;
    /**
     * @brief numberOfCourses number of courses whose learning have been started.
     */
    int numberOfCourses;
    /**
     * @brief totalLearningTime total learning time.
     */
    QTime totalLearningTime;
    /**
     * @brief configFilePath path to configuration data.
     */
    std::string configFilePath;
    /**
     * @brief coursesListFilePath path to file with data about courses.
     */
    std::string coursesListFilePath;
};

#endif // CONFIG_H
